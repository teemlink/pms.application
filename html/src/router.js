import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)


const router = new Router({
  mode: 'hash',
  routes: [
    // {
    //   path: '/',
    //   redirect: '/login',
    // },
    // 项目管理路由
    {
      path: '/project_list',
      name: 'project_list',
      component: () => import('./components/project_list.vue')
    },
    {
      path: '/card',
      name: 'card',
      component: () => import('./components/card.vue')
    },
    {
      path: '/mapview',
      name: 'mapview',
      component: () => import('./components/view_mapview.vue')
    },
    {
      path: '/project_card',
      name: 'project_card',
      component: () => import('./components/project_card.vue')
    },
    
  ]
});
// router.beforeEach((to, from, next) => {
//   next()
// })
export default router;