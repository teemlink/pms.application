module.exports = {
  // 公用部分
  placeholder:{
    select: "请选择",
    content: "请输入内容",
  },
  btns: {
    sure:'确认',
    cancel:'取消', 
  },
  delete:'删除',
  map:'地图',
  statellite:'卫星',
  address:'地址',
  close_route:'关闭路线',
  open_route:'开启路线',
  addr_list:'详细地址列',
  sign:'标记该点位置',
  nosign:'取消标记',
  enlarge:'放大',
  shrink:'缩小',
  total:'总条数',
  msg: {
    delConfirm: "确认删除?",
    tip: "提示",
    confirm: "确定",
    cancel: "取消",
  },
  success: "成功",
};
