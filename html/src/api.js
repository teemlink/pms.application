// 引入 axios
import axios from 'axios';

// cp是全局变量，之前是通过使用process.env.NODE_ENV==='production'来判断打包的是哪一个路径，在tomcat下是..(由于在Tomcat是domain在obpm里面，但是要找到根目录的路径)，在本地是obpm
const obpmFilePath = obpmConfig.obpmFilePath
const contextPath = obpmConfig.contextPath; 
const instance = axios.create({
});
instance.interceptors.request.use(
    config => {
        if(config.method.toLowerCase() == 'put' || config.method.toLowerCase() == 'post'){
            // config.headers['adminToken'] = localStorage.getItem('adminToken');
            // if(config.data && config.data.loginpwd && config.data.loginpwd.length > 60){
            //     Message.error('密码长度不能大于60位')
            //     return false
            // }
            return config
        }else{
            // config.headers['adminToken'] = localStorage.getItem('adminToken');
            return config
        }
    },
    err => {
        return Promise.reject(err)
    }
);

//http response 拦截器
instance.interceptors.response.use(
    response => {
        console.log("axios.interceptors.response");
        return response;
    },
    error => {
        //未登录
        if (error.response.status == 401) {
            window.location = "index.html#/login";
        }
        else {
            return Promise.reject(error)
        }
    }
);

instance.defaults.withCredentials = true;

/**
 * 项目管理
 */

///runtime/upload?applicationId={applicationId}&allowedTypes={allowedTypes}&fieldId={fieldId}&fileSaveMode={fileSaveMode}&path={path}&actionType={actionType}
export function uploadFile(files, appId, allowedTypes, fieldId, fileSaveMode, path, actionType, sourceFileId,docId,{ onSucess, onError }, callback1, uid) {
    let accessToken = localStorage.getItem('accessToken')
    axios({
        headers: {
            "content-type": "multipart/form-data;boundary=" + Math.random(),
            "accessToken" : accessToken
        },
        data: files,
        url: contextPath + "/runtime/upload?applicationId=" +
            appId +
            "&allowedTypes=" +
            allowedTypes +
            "&fieldId=" +
            fieldId +
            "&fileSaveMode=" +
            fileSaveMode +
            "&path=" +
            path +
            "&actionType=" +
            actionType +
            "&sourceFileId=" +
            sourceFileId +
            "&docId=" +
            docId,
        method: "post",
        onUploadProgress:(progressEvent) => { 
            let complete = (progressEvent.loaded / progressEvent.total * 100 | 0) + '%';
            let num =  (progressEvent.loaded / progressEvent.total * 100 | 0);
            let obj = {
                complete: complete,
                num: num,
                uid: uid,
            }
            callback1(obj);
        }
    }).then(function (response) {
        if(onSucess) onSucess(response);
    }).catch(
        function (error) {
            if(onError) onError(error);
        }
    )  
}
//获取视图列
export function getViewTemplate (appId, viewId, { onSucess, onError }) {
    instance.get(contextPath + '/runtime/' + appId + '/views/' + viewId + '/template',).then(function (response) {
        if (onSucess) onSucess(response);
    }).catch(
        function (error) {
            if (onError) onError(error);
        }
    );
}
/**
 * 获取视图数据
 */
export function getViewData (appId, viewId, params, data, { onSucess, onError }) {
    let urlParams = '';
    let parentId = params.parentId ? params.parentId : '';
    let sortCol = params.sortCol ? params.sortCol : '';
    let sortStatus = params.sortStatus ? params.sortStatus : '';
    let currpage = params.currpage ? params.currpage : '1';
    let lines = params.lines ? params.lines : '';
    let treedocid = params.treedocid ? params.treedocid : '';
    let parentNodeId = params.parentNodeId ? params.parentNodeId : '';
    let docid = params.docid ? params.docid : '';
    let fieldid = params.fieldid ? params.fieldid : '';
    let isRelate = params.isRelate ? params.isRelate : '';
    let startDate = params.startDate ? params.startDate : '';
    let endDate = params.endDate ? params.endDate : '';
    let exparams = params.exparams ? params.exparams : '';
    let parentParam = params.parentParam ? params.parentParam : '';

    urlParams += 
        '?parentId=' + parentId + 
        '&sortCol=' + sortCol +
        '&sortStatus=' + sortStatus + 
        '&_currpage=' + currpage +
        '&lines=' + lines + 
        '&treedocid=' + treedocid +
        '&parentNodeId=' + parentNodeId + 
        '&_docid=' + docid +
        '&_fieldid=' + fieldid + 
        '&isRelate=' + isRelate +
        '&startDate=' + startDate + 
        '&endDate=' + endDate + 
        '&parentParam=' + parentParam; 
    if (data && exparams) {
        data = Object.assign(data, exparams);
    }
    instance.post(contextPath + '/runtime/' + appId + '/views/' + viewId + '/documents' + urlParams, data ? data : {}).then(function (response) {
        if (onSucess) onSucess(response);
    }).catch(
        function (error) {
            if (onError) onError(error);
        }
    );
}
export function getProjectTemplateList(appName,userId,{ onSucess, onError }) {
    instance.get(obpmFilePath + '/magic-api/'+appName+'/project_template_list?userId='+userId).then(function (response) {
        if (onSucess) onSucess(response);
    }).catch(
        function (error) {
            if (onError) onError(error);
        }
    );
}
/**
 * 项目模板保存
 */
export function createProjectTemplate(appName,data,{ onSucess, onError }) {
    instance.post(obpmFilePath + '/magic-api/'+appName+'/create_project_template',data).then(function (response) {
        if (onSucess) onSucess(response);
    }).catch(
        function (error) {
            if (onError) onError(error);
        }
    );
}
// 项目列表
export function getProject_list(appName,data,{ onSucess, onError }) {
    instance.post(obpmFilePath + '/magic-api/'+appName+'/project_list',data).then(function (response) {
        if (onSucess) onSucess(response);
    }).catch(
        function (error) {
            if (onError) onError(error);
        }
    );
}
/**
 * 模板类型选项
 */
export function projectTemplateType(appName,{ onSucess, onError }) {
    instance.get(obpmFilePath + '/magic-api/'+appName+'/project_template_type_list').then(function (response) {
        if (onSucess) onSucess(response);
    }).catch(
        function (error) {
            if (onError) onError(error);
        }
    );
}
/**
 * 项目模板卡片列表
 */
export function getCardList(appName,data,{ onSucess, onError }) {
    instance.post(obpmFilePath + '/magic-api/'+appName+'/project_template_card_list',data).then(function (response) {
        if (onSucess) onSucess(response);
    }).catch(
        function (error) {
            if (onError) onError(error);
        }
    );
}
/**
 * 卡片保存
 */
export function createCard(appName,data,{ onSucess, onError }) {
    instance.post(obpmFilePath + '/magic-api/'+appName+'/create_card',data).then(function (response) {
        if (onSucess) onSucess(response);
    }).catch(
        function (error) {
            if (onError) onError(error);
        }
    );
}
 /**
 * 卡片保存
 */
export function deleteCard(appName,data,{ onSucess, onError }) {
    instance.post(obpmFilePath + '/magic-api/'+appName+'/delete_card',data).then(function (response) {
        if (onSucess) onSucess(response);
    }).catch(
        function (error) {
            if (onError) onError(error);
        }
    );
}
/**
 * 卡片创建任务
 */
export function credateCardTask(appName,data,{ onSucess, onError }) {
    instance.post(obpmFilePath + '/magic-api/'+appName+'/credate_card_task',data).then(function (response) {
        if (onSucess) onSucess(response);
    }).catch(
        function (error) {
            if (onError) onError(error);
        }
    );
}
/**
 * 卡片任务查询
 */
export function cardTaskList(appName,data,{ onSucess, onError }) {
    instance.post(obpmFilePath + '/magic-api/'+appName+'/card_task_list',data).then(function (response) {
        if (onSucess) onSucess(response);
    }).catch(
        function (error) {
            if (onError) onError(error);
        }
    );
}
