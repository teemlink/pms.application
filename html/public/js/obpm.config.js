/**
 * 5.0
 * **/
let obpmConfig = {
  contextPath: "/obpm/api",
  statiContextPath: "/static",
  obpmFilePath: "/obpm",
  signonContextPath: "/signon",
  messagePath: "/message/api",
  converterPath:"/converter/api",
  kmsContextPath:"/kms/api",
  kmsFilePath:"/kms",
  magicHtmlPath:"/obpm/html"
}